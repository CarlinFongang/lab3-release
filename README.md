# Gitlab-CI | tag de l'artifact docker et push sur un registry public : 

<p align="center">
Please find the specifications by clicking
  <a href="https://github.com/eazytrainingfr/alpinehelloworld.git" alt="Crédit : eazytraining.fr" >
  </a>
</p>
------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


<img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 

LinkedIn : https://www.linkedin.com/in/carlinfongang/

# Objectif
dans ce lab, il faudra : 
1. crée un nouveau job qui permettra de tager l'image produire après le build et 
2. Pusher l'image taggé sur le registry de gitlab

## Création d'un variable 
>![Alt text](img/image.png)

## Récupération de la valeur de nom dans container registry
>![Alt text](img/image-1.png)

## Rajout de la variable 
>![Alt text](img/image-2.png)

## Run pipeline
>![Alt text](img/image-3.png)

weldone of pipeline 
>![Alt text](img/image-4.png)

## Check of registry
>![Alt text](img/image-5.png)
>![Alt text](img/image-6.png)

